# Backend - Proyecto de cafetería
Proyecto solicitado en el bootcamp

# Que necesitamos?
- Python 3+
- MongoDB
- Django y DRF

# Sobre el codigo:
Separamos el codigo en ramas, segun las clases desarolladas

Las distintas secciones estan contenidas en ramas:
- **mongodb-integration**: Proyecto base de cafeteria integrado con Mongo, con API básica de Productos utilizando ViewSets.
- **ejemplo-relaciones**: Ejemplo de relaciones en MongoDB: Pedidos y productos.
- **authentication**: Autenticacion de endpoints con JWT Token.
- **authentication-with-roles**: Autorizacion de endpoints con JWT Token.
- **front-end-integration**: Integracion con el proyecto front end, Se agrega CORS, modifica duracion del token para simplificar desarrollo, se creo un nuevo endpoint para obtener la informacion del user. 
  
Para cambiar a una rama diferente, utilice el siguiente comando Git:
`git checkout nombre-rama  `

## Instalación de dependencias
Para instalar las dependencias del proyecto, siga estos pasos:
1. Abra una terminal dentro del proyecto, en la misma ubicación que el archivo *requirements.txt.*
2. Active el virtualenv del proyecto.
2. Ejecute el siguiente comando pip para instalar las dependencias del proyecto desde el archivo *requirements.txt*:
`pip install -r requirements.txt`

## Correr migraciones
Para crear la base de datos localmente deben correr el siguiente comando
`python .\manage.py migrate`

# Arquitectura general del proyecto cafeteria
![Diagrama de Arquitectura](arquitectura_general.jpeg)

# Funciones por rol
![Componentes](funciones_por_rol.jpeg)
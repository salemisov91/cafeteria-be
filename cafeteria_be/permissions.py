from rest_framework import permissions
from django.contrib.auth.models import Group
    
class IsRecepcionista(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            return request.user.groups.get().name == 'recepcionista'
        except Group.DoesNotExist:
            return False

    
class IsCocinero(permissions.BasePermission):
    def has_permission(self, request, view):
        return request.user.groups.get().name == 'cocinero'
    
class IsRecepcionistaOrCocinero(permissions.BasePermission):
    def has_permission(self, request, view):
        role = request.user.groups.get().name
        return role == 'recepcionista' or role == 'cocinero'
    
class IsAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        try:
            return request.user.groups.get().name == 'admin'
        except Group.DoesNotExist:
            return False

class IsRecepcionistaOrCocineroOrAdmin(permissions.BasePermission):
    def has_permission(self, request, view):
        role = request.user.groups.get().name
        return role == 'recepcionista' or role == 'cocinero' or role == 'admin'
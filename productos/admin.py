from django.contrib import admin
from . import models
class Productoadmin(admin.ModelAdmin):
    list_display = ["id","nombre","precio"]
    list_filter = ["nombre"]
    search_fields = ["nombre"]
admin.site.register(models.Producto, Productoadmin)
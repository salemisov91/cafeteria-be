from rest_framework import viewsets
from .models import Producto
from .serializers import ProductoSerializer
from cafeteria_be.permissions import IsCocinero, IsAdmin, IsRecepcionista, IsRecepcionistaOrCocinero, IsRecepcionistaOrCocineroOrAdmin
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import action

class ProductosViewSet(viewsets.ModelViewSet):
    # Minimamente hay que pasar queryset y serializer_class
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = [IsRecepcionista, IsAdmin] # Instancia y retorna la lista de permisos que esta vista requiere

    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list' or self.action == 'update':
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        elif self.action == 'productos': # Endpoint custom
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        serializer = ProductoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    @action(detail=False, methods=['delete'])
    def borrar_todos_los_productos(self, request):
        productos_borrados, _ = Producto.objects.all().delete()
        return Response({'productos_borrados': productos_borrados}, status=status.HTTP_200_OK)
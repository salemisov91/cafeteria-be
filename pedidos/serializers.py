from rest_framework import serializers
from .models import Pedido
import uuid


class PedidoSerializer(serializers.ModelSerializer):
    lista_productos = serializers.ListField(child=serializers.DictField())

    class Meta:
        model = Pedido
        fields = ['id', 'mesa', 'estado', 'lista_productos']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        lista_productos = representation.get('lista_productos', [])
        representation['lista_productos'] = lista_productos
        return representation


    def to_internal_value(self, data):
        if 'id' in data:
            data['id'] = uuid.UUID(data['id'])
        return super().to_internal_value(data)

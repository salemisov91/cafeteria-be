from django.contrib import admin
from . import models
class Pedidoadmin(admin.ModelAdmin):
    list_display = ["mesa","estado"]
    list_filter = ["mesa","estado"]
    search_fields = ["mesa","estado"]
admin.site.register(models.Pedido, Pedidoadmin)
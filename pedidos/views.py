from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from .models import Pedido
from .serializers import PedidoSerializer
from productos.models import Producto
from productos.serializers import ProductoSerializer
from cafeteria_be.permissions import IsCocinero, IsRecepcionista, IsRecepcionistaOrCocinero, IsRecepcionistaOrCocineroOrAdmin


class PedidosViewSet(viewsets.ModelViewSet):
    queryset = Pedido.objects.all()
    serializer_class = PedidoSerializer
    
    def get_permissions(self):
        permission_classes = []
        if self.action == 'retrieve' or self.action == 'list' or self.action == 'update':
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        elif self.action == 'create' or self.action == 'update' or self.action == 'partial_update' or self.action == 'destroy':
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        elif self.action == 'pedidos': # Endpoint custom
            permission_classes = [IsRecepcionistaOrCocineroOrAdmin]
        return [permission() for permission in permission_classes]
    
    def get_queryset(self):
        queryset = Pedido.objects.all()
        estado = self.request.query_params.get('estado', None)
        if estado is not None:
            queryset = queryset.filter(estado=estado)
        return queryset

    @action(detail=True, methods=['get'])
    def productos(self, request, pk=None):
        id_pedido = self.kwargs['pk']
        try:
            pedido = Pedido.objects.get(pk=id_pedido)
            lista_productos = []
            for producto_id in pedido.lista_productos:
                lista_productos.append(Producto.objects.get(pk=producto_id))
            return Response(ProductoSerializer(lista_productos, many=True).data, 
                            status=status.HTTP_200_OK)
        except Exception as e:
            print(e)
            return Response(None, status=status.HTTP_500_INTERNAL_SERVER_ERROR)


    @action(detail=True, methods=['post'])
    def add_pedido(self, request, pk=None):
        producto_ids = [producto['id'] for producto in request.data['lista_productos']]
        try:
            pedido = Pedido.objects.get(pk=pk)
            pedido.lista_productos += producto_ids
        except Pedido.DoesNotExist:
            pedido = Pedido(mesa=request.data['mesa'], lista_productos=producto_ids)

        pedido.save()
        serializer = PedidoSerializer(pedido)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    
    @action(detail=True, methods=['delete'])
    def delete_pedido(self, request, pk=None):
        pedido = Pedido.objects.get(pk=pk)
        pedido.delete()
        return Response(status=204)
    
    @action(detail=False, methods=['delete'])
    def borrar_todos_los_pedidos(self, request):
        pedidos_borrados, _ = Pedido.objects.all().delete()
        return Response({'pedidos_borrados': pedidos_borrados}, status=status.HTTP_200_OK)
    
    @action(detail=True, methods=['put'])
    def update_pedido(self, request, pk=None):
        try:
            pedido = Pedido.objects.get(pk=pk)
            serializer = PedidoSerializer(pedido, data=request.data, partial=True)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data)
            else:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        except Pedido.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
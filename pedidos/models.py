from djongo import models
from uuid import uuid4

class Pedido(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    mesa=models.CharField(max_length=100)
    estado=models.CharField(max_length=50)
    lista_productos = models.JSONField()